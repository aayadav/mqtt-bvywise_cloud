﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Npgsql;
namespace BvywiseMQTT
{
    class clsPostgreSQLBasic
    {
        NpgsqlConnection m_conn;
        //NpgsqlConnection p_conn;
        //NpgsqlCommand m_cmd;
        //NpgsqlDataAdapter npda;
        clsLogs objLogger;
        string strConn = string.Empty;

        public clsPostgreSQLBasic(string logfilename, string strConnection, string Dbname)
        {
            objLogger = new clsLogs(logfilename);
            fnCreatePgDatabase(strConnection, Dbname);
            strConn = strConnection + "Database=" + "\'" + Dbname + "\';Integrated Security=true;Pooling=true;";
            //strConn = strConnection + "Database=" + "\'" + Dbname + "\';Integrated Security=true;Pooling=true;Minimum Pool Size=0;Maximum Pool Size=100;";

            m_conn = new NpgsqlConnection(strConn);
            m_conn.Open();
            //p_conn = new NpgsqlConnection(strConn);
            //p_conn.Open();
        }
        public void fnCreatePgDatabase(string connStr, string strDatabaseName)
        {
            objLogger.fnWriteLog("Entry", "fnCreatePgDatabase", "Info");
            bool dbExists = false;
            try
            {
                //connStr = "Server=localhost;Port=5432;User Id=postgres;Password=techm@16;";
                using (NpgsqlConnection m_conn = new NpgsqlConnection(connStr))
                {
                    string sqlCreateDBQuery = "SELECT 1 FROM pg_database WHERE datname=" + "\'" + strDatabaseName + "\'";
                    using (NpgsqlCommand cmd = new NpgsqlCommand(sqlCreateDBQuery, m_conn))
                    {
                        if (m_conn.State != ConnectionState.Open)
                        {
                            m_conn.Open();
                        }
                        dbExists = cmd.ExecuteScalar() != null;
                        cmd.Dispose();
                    }
                    if (!dbExists)
                    {
                        string strDBQuery = "CREATE DATABASE " + strDatabaseName;
                        using (NpgsqlCommand m_createdb_cmd = new NpgsqlCommand(strDBQuery, m_conn))
                        {
                            if (m_conn.State != ConnectionState.Open) m_conn.Open();
                            int i = m_createdb_cmd.ExecuteNonQuery();
                            m_createdb_cmd.Dispose();
                        }
                        objLogger.fnWriteLog("Create Database", "fnCreatePgDatabase", "Debug");
                    }
                    m_conn.Close();
                    m_conn.Dispose();
                }
            }
            catch (Exception ex)
            {
                objLogger.fnWriteLog(ex.Message + " Inner Ex= " + ex.InnerException, "fnCreatePgDatabase", "Error");
            }
            objLogger.fnWriteLog("Exit", "fnCreatePgDatabase", "Info");
        }

        public List<string> fnGetDeviceParaList(DataTable dtParamterData)
        {
            objLogger.fnWriteLog("Entry", "fnGetDeviceParaList", "Info");
            List<string> lstParaColumnNm = new List<string>();
            try
            {
                int iRows = dtParamterData.Rows.Count;
                for (int i = 0; i < iRows; i++)
                {
                    if (iRows > 0)
                    {
                        lstParaColumnNm.Add(Convert.ToString(dtParamterData.Rows[i]["ParameterName"]).Replace(" ", "_"));
                    }
                }
            }
            catch (Exception ex)
            {
                objLogger.fnWriteLog(ex.Message + " Inner Ex= " + ex.InnerException, "fnGetDeviceParaList", "Error");
            }
            objLogger.fnWriteLog("Exit", "fnGetDeviceParaList", "Info");
            return lstParaColumnNm;
        }

        public string fnCreateTableQuery(List<string> lststrParameters, string strTableName, string strpalletchange, string strCommunicationtype)
        {
            objLogger.fnWriteLog("Entry", "fnCreateTableQuery", "Info");
            string strQuery = string.Empty;
            try
            {
                strQuery = "CREATE TABLE " + strTableName + "(ID SERIAL PRIMARY KEY";
                for (int i = 0; i < lststrParameters.Count; i++)
                {
                    strQuery += "," + lststrParameters[i] + " TEXT ";
                    if (lststrParameters.Count == i)
                    {
                        strQuery += lststrParameters[i] + " TEXT ";
                    }
                }
                if (strCommunicationtype == "ExportToExcel")
                {
                    strQuery += ",ProducingTime TEXT,IdelTime TEXT,AddedDateTime TEXT,ActivityStatus TEXT,PlcState TEXT";
                }
                else
                {
                    strQuery += ",AddedDateTime TEXT,ActivityStatus TEXT";
                }
                if (!string.IsNullOrEmpty(strpalletchange))
                {
                    strQuery += ",PalletChangeCount TEXT";
                }
                strQuery += " ,timestamp timestamp default current_timestamp);";
            }
            catch (Exception ex)
            {
                objLogger.fnWriteLog(ex.Message + " Inner Ex= " + ex.InnerException, "fnCreateTableQuery", "Error");
            }
            objLogger.fnWriteLog("Exit", "fnCreateTableQuery", "Info");
            return strQuery;
        }

        public void fnCreateTables(string connStr, string strDatabaseNM, string strTableNM, string strCreateTblQ)
        {
            objLogger.fnWriteLog("Entry", "fnCreateTables", "Info");
            //connStr= "Server=localhost;Port=5432;User Id=postgres;Password=techm@16;"
            string newConnStr = connStr + "Database=" + strDatabaseNM;
            objLogger.fnWriteLog("Database Connection String= " + newConnStr, "fnCreateTables", "Debug");
            bool existstable = false;
            try
            {
                using (NpgsqlConnection m_conn = new NpgsqlConnection(newConnStr))
                {
                    if (m_conn.State != ConnectionState.Open) m_conn.Open();
                    string sqlStatement = @"SELECT 1 FROM pg_tables WHERE tablename =" + "\'" + strTableNM + "\'";
                    using (NpgsqlCommand m_createtbl_cmd = new NpgsqlCommand(sqlStatement, m_conn))
                    {
                        if (m_createtbl_cmd.ExecuteScalar() == null)
                        {
                            existstable = true;
                            objLogger.fnWriteLog("Create New Table", "fnCreateTables", "Debug");
                        }
                        else
                        {
                            existstable = false;
                            objLogger.fnWriteLog("Table already exist", "fnCreateTables", "Debug");

                        }
                        m_createtbl_cmd.Dispose();
                    }
                    if (existstable)
                    {
                        using (NpgsqlCommand m_createtbl_cmd = new NpgsqlCommand(strCreateTblQ, m_conn))
                        {
                            m_createtbl_cmd.ExecuteNonQuery();
                            objLogger.fnWriteLog("Table Created Successfully.", "fnCreateTables", "Debug");
                            m_createtbl_cmd.Dispose();
                        }
                    }
                    m_conn.Close();
                    m_conn.Dispose();
                }
            }
            catch (Exception ex)
            {
                objLogger.fnWriteLog(ex.Message + " Inner Ex= " + ex.InnerException, "fnCreateTables", "Error");
            }
            objLogger.fnWriteLog("Exit", "fnCreateTables", "Info");
        }

        public List<string> fnCreateProductionTable(string connStr, string strDatabaseNM, string strTableNM)
        {
            objLogger.fnWriteLog("Entry", "fnCreateProductionTable", "Info");
            List<string> strProdList = new List<string>();
            strProdList.Add("DeviceId");
            strProdList.Add("PartCount");
            strProdList.Add("ProgramName");
            string newConnStr = connStr + "Database=" + "\'" + strDatabaseNM + "\'";
            bool existstable = false;
            try
            {
                using (NpgsqlConnection m_conn = new NpgsqlConnection(newConnStr))
                {
                    if (m_conn.State != ConnectionState.Open) m_conn.Open();
                    string sqlStatement = @"SELECT 1 FROM pg_tables WHERE tablename =" + "\'" + strTableNM + "_ProdCount" + "\'";
                    using (NpgsqlCommand m_createtbl_cmd = new NpgsqlCommand(sqlStatement, m_conn))
                    {
                        if (m_createtbl_cmd.ExecuteScalar() == null)
                        {
                            existstable = true;
                            objLogger.fnWriteLog("Production Create New Table", "fnCreateProductionTable", "Debug");
                        }
                        else
                        {
                            existstable = false;
                            objLogger.fnWriteLog("Production Table already exist", "fnCreateProductionTable", "Debug");

                        }
                        m_createtbl_cmd.Dispose();
                    }
                    string strCreateTblQ = "CREATE TABLE " + strTableNM + "_ProdCount" + "(ID SERIAL PRIMARY KEY,DeviceId TEXT,PartCount TEXT,ProgramName TEXT,AddedDateTime TEXT,timestamp timestamp default current_timestamp)";
                    objLogger.fnWriteLog("Prouction Table Query= " + strCreateTblQ, "fnCreateProductionTable", "Debug");

                    if (existstable)
                    {
                        using (NpgsqlCommand m_createtbl_cmd = new NpgsqlCommand(strCreateTblQ, m_conn))
                        {
                            m_createtbl_cmd.ExecuteNonQuery();
                            objLogger.fnWriteLog("Prouction Table Created Successfully.", "fnCreateProductionTable", "Debug");
                            m_createtbl_cmd.Dispose();
                        }
                    }
                    m_conn.Close();
                    m_conn.Dispose();
                }
            }
            catch (Exception ex)
            {
                objLogger.fnWriteLog(ex.Message + " Inner Ex= " + ex.InnerException, "fnCreateProductionTable", "Error");
            }
            objLogger.fnWriteLog("Exit", "fnCreateProductionTable", "Info");
            return strProdList;
        }

        public bool fnInsertUpdateDelete(string strQuery)
        {
            objLogger.fnWriteLog("Entry", "fnInsertUpdateDelete", "Info");
            bool bln = false;
            //string newConnStr = connStr + "Database=" + "\'" + strDatabaseNM + "\'";
            //NpgsqlConnection m_conn = new NpgsqlConnection(strConn);
            try
            {
                //Thread.Sleep(10);
                //using (NpgsqlConnection m_conn = new NpgsqlConnection(strConn))
                {

                    if (m_conn.State != ConnectionState.Open)
                        m_conn.Open();
                    using (NpgsqlTransaction transaction = m_conn.BeginTransaction())
                    {
                        NpgsqlCommand m_cmd = new NpgsqlCommand(strQuery, m_conn);
                        m_cmd.ExecuteNonQuery();
                        bln = true;
                        m_cmd.Dispose();
                        transaction.Commit();
                        //transaction.Dispose();
                    }
                    //m_conn.Close();
                    //m_conn.Dispose();
                }
            }
            catch (Exception ex)
            {
                bln = false;
                //m_conn.Close();
                //m_conn.Dispose();
                objLogger.fnWriteLog(ex.Message + " Inner Ex= " + ex.InnerException, "fnInsertUpdateDelete", "Error");
            }
            objLogger.fnWriteLog("Exit", "fnInsertUpdateDelete", "Info");
            return bln;
        }

        public void fnCloudGetAllDetails(string strQuery, out DataTable dtResultData)
        {
            objLogger.fnWriteLog("Entry", "fnCloudGetAllDetails", "Info");
            dtResultData = new DataTable();
            // NpgsqlConnection m_conn = new NpgsqlConnection(strConn);
            try
            {
                //Thread.Sleep(10);
                using (NpgsqlConnection p_conn = new NpgsqlConnection(strConn))
                {
                    if (p_conn.State != ConnectionState.Open)
                        p_conn.Open();
                    using (NpgsqlTransaction transaction = p_conn.BeginTransaction())
                    {
                        NpgsqlDataAdapter npda = new NpgsqlDataAdapter(strQuery, p_conn);
                        npda.Fill(dtResultData);
                        npda.Dispose();
                        transaction.Commit();
                        transaction.Dispose();
                    }
                    //m_conn.Close();
                    //m_conn.Dispose();
                }
            }
            catch (Exception ex)
            {
                //m_conn.Close();
                //m_conn.Dispose();
                objLogger.fnWriteLog(ex.Message + "###" + ex.InnerException, "fnCloudGetAllDetails", "Error");
            }
            objLogger.fnWriteLog("Exit", "fnCloudGetAllDetails", "Info");
        }

    }
}
