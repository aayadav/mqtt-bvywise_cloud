﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BvywiseMQTT
{
    public class clsCloudMediator
    {
        clsCloudDataLayer objclsCloudDataLayer;
        BvywiseMQ objBvywiseMQ;
        clsLogs objLogger;
        string strlogfilename = string.Empty;

        public clsCloudMediator(string strFN)
        {
            strlogfilename = strFN;
            objLogger = new clsLogs(strFN);
            objclsCloudDataLayer = new clsCloudDataLayer(strlogfilename);
        }

        string strTblDeviceName = string.Empty;
        string strDbMachineName = string.Empty;
        string strCommunicationType = string.Empty;
        DataTable dtParamterData;
        Thread thrBvywiseMq;
        Thread thrCloudOtherData;
        Thread thrCloudThreadActivitystatus;
        Thread thrCheckConnection;

        private void fnLogtypeEnableDisable()
        {
            string strEnableLogging = string.Empty;
            try
            {
                strEnableLogging = Convert.ToString(dtCommanData.Rows[0]["LogType"]);
                if (strEnableLogging != null)
                {

                    if (strEnableLogging.ToLower() == "info")
                    {
                        clsCloudDataLayer.strLogType = "Info";
                    }
                    else if (strEnableLogging.ToLower() == "debug")
                    {
                        clsCloudDataLayer.strLogType = "Debug";
                    }
                }
                else
                {
                    clsCloudDataLayer.strLogType = "Error";
                }
            }
            catch
            {

            }
        }
        //BvywiseMQ[] objBvywiseMQ1;
        public void fnStartCloudThread()
        {
            objLogger.fnWriteLog("Entry", "fnStartCloudThread", "Info");
            try
            {
                fnBindAllDatabasesAndTables();

                fnLogtypeEnableDisable();
                //objBvywiseMQ1 = new BvywiseMQ[dtDevices.Rows.Count];
                for (int i = 0; i < dtDevices.Rows.Count; i++)
                {
                    strTblDeviceName = Convert.ToString(dtDevices.Rows[i]["DeviceName"]);
                    strDbMachineName = Convert.ToString(dtDevices.Rows[i]["MachineName"]);

                    //strTblDeviceName = strTblDeviceName.Replace(" ", "_");

                    objclsCloudDataLayer = new clsCloudDataLayer(strlogfilename);


                    string strDeviceId = Convert.ToString(dtDevices.Rows[i]["DeviceId"]);
                    string strParamterQuery = "Select * from tbl_ParameterConfig where DeviceId='" + strDeviceId + "'";

                    bool flag = objclsCloudDataLayer.BindDataGridDetails(out dtParamterData, strConfigDBName, strParamterQuery);
                    objLogger.fnWriteLog("Parameter Details Flag= " + flag, "BindDataGridDetails", "Debug");

                    string strDevWiseProtocol = Convert.ToString(dtDevices.Rows[i]["ControlMake"]);
                    string strPalletChangeProgName = Convert.ToString(dtDevices.Rows[i]["PalletProgramName"]);
                    //string strProgramNumber = Convert.ToString(dtDevices.Rows[i]["ProgramNumber"]);
                    //string strPartCount = Convert.ToString(dtDevices.Rows[i]["ProgNoDB"]);
                    strlogfilename = Application.StartupPath + "\\Logs\\" + strTblDeviceName + "_CloudCall" + "_0.log";
                    string strProductionFN = Application.StartupPath + "\\Logs\\" + strTblDeviceName + "_CloudProductionCall" + "_0.log";
                    //objClsCloudCall = new clsCloudCall(strFN, strProductionFN, strDeviceId, strTblDeviceName, strDbMachineName, Convert.ToInt32(dtDevices.Rows.Count), strCustomerName, strBasicUrl, strChkInternet, Timeout);
                    //objBvywiseMQ = new BvywiseMQ(dtParamterData, strlogfilename, strDeviceId, strTblDeviceName, strDbMachineName + "_" + strCustomerName, Convert.ToInt32(dtDevices.Rows.Count), strCustomerName, strBasicUrl, strChkInternet, Timeout, strPalletChangeProgName, strPartCount, dtCommanData, strProgramNumber, strKeepAlive);
                    objBvywiseMQ = new BvywiseMQ(dtParamterData, strlogfilename, strDeviceId, strTblDeviceName, strDbMachineName, Convert.ToInt32(dtDevices.Rows.Count), strCustomerName, strBasicUrl, strChkInternet, Timeout, strPalletChangeProgName, dtCommanData, strCommunicationType, strKeepAlive);

                    if (objBvywiseMQ.fnConnect())
                    {
                        
                            thrBvywiseMq = new Thread(new ThreadStart(objBvywiseMQ.fnFeatureData));
                            thrBvywiseMq.Start();
                            //Thread.Sleep(200);
                            //thrCloudOtherData = new Thread(new ThreadStart(objBvywiseMQ.fnThreadProductionAndAlarm));
                            //thrCloudOtherData.Start();
                            Thread.Sleep(200);
                            thrCheckConnection = new Thread(new ThreadStart(objBvywiseMQ.fnCheckConnection));
                            thrCheckConnection.Start();
                        
                    }
                    thrCloudThreadActivitystatus = new Thread(new ThreadStart(fnCheckThreadState));
                    thrCloudThreadActivitystatus.Start();

                }
                // objLogger.fnWriteLog("Exit", "fnStartCloudThread", "Error");
            }
            catch (Exception ex)
            {
                objLogger.fnWriteLog(ex.Message + " " + ex.InnerException, "fnStartCloudThread", "Error");
            }
            objLogger.fnWriteLog("Exit", "fnStartCloudThread", "Info");
        }

        public void fnCheckThreadState()
        {
            objLogger.fnWriteLog("Entry", "fnCheckThreadState", "Info");

            while (true)
            {
                Thread.Sleep(10000);
                try
                {
                    objLogger.fnWriteLog("thrBvywiseMq State==" + thrBvywiseMq.ThreadState, "fnCheckThreadState", "Debug");

                    if (thrBvywiseMq == null || !thrBvywiseMq.IsAlive || thrBvywiseMq.ThreadState == ThreadState.Stopped)
                    {
                        objLogger.fnWriteLog("thrBvywiseMq Finished and Start", "fnCheckThreadState", "Debug");
                        thrBvywiseMq.Start();
                    }
                    //else if (thrCloudOtherData == null || !thrCloudOtherData.IsAlive || thrCloudOtherData.ThreadState == ThreadState.Stopped)
                    //{
                    //    objLogger.fnWriteLog("thrCloudOtherData Finished and Start", "fnCheckThreadState", "Debug");
                    //    thrCloudOtherData.Start();
                    //}
                    if (thrCheckConnection == null || !thrCheckConnection.IsAlive || thrCheckConnection.ThreadState == ThreadState.Stopped)
                    {
                        objLogger.fnWriteLog("thrCheckConnection Finished and Start", "fnCheckThreadState", "Debug");
                        thrCheckConnection.Start();
                    }
                }
                catch (Exception ex)
                {
                    objLogger.fnWriteLog(ex.Message, "fnCheckThreadState", "Error");
                }
                objLogger.fnWriteLog("Exit", "fnCheckThreadState", "Info");
            }
        }

        string strConfigDBName = string.Empty;
        string strDevicesQuery = string.Empty;
        string strCommanDataquery = string.Empty;
        DataTable dtDevices;
        DataTable dtCommanData;
        string strCustomerName = string.Empty;
        string strBasicUrl = string.Empty;
        string strChkInternet = string.Empty;
        int Timeout = 0;
        string strProtocolType = string.Empty;
        string strKeepAlive = string.Empty;

        public void fnBindAllDatabasesAndTables()
        {
            objLogger.fnWriteLog("Entry", "fnBindAllDatabasesAndTables", "Info");
            try
            {
                strConfigDBName = "DB_Configuration";
                strDevicesQuery = "Select * from tbl_DeviceConfig";
                strCommanDataquery = "Select * from tbl_CommanConfigSettings";
                //  strParamterQuery = "Select * from tbl_ParameterConfig ";
                //strParamterQuery= "Select * from tbl_ParameterConfig where DeviceId='" + DeviceId + "'";

                bool flag = objclsCloudDataLayer.BindDataGridDetails(out dtDevices, strConfigDBName, strDevicesQuery);
                objLogger.fnWriteLog("Device Details Flag= " + flag, "BindDataGridDetails", "Debug");

                bool flag1 = objclsCloudDataLayer.BindDataGridDetails(out dtCommanData, strConfigDBName, strCommanDataquery);
                objLogger.fnWriteLog("Common Details Flag= " + flag1, "BindDataGridDetails", "Debug");

                strCustomerName = Convert.ToString(dtCommanData.Rows[0]["CustomerName"]);
                strBasicUrl = Convert.ToString(dtCommanData.Rows[0]["CloudUrl"]);
                strChkInternet = Convert.ToString(dtCommanData.Rows[0]["CheckInternet"]);
                strCommunicationType = Convert.ToString(dtCommanData.Rows[0]["CommunicationType"]);
                Timeout = Convert.ToInt32(dtCommanData.Rows[0]["SendUrlTimeout"]);
                strProtocolType = Convert.ToString(dtCommanData.Rows[0]["ProtocolType"]);
                strKeepAlive = Convert.ToString(dtCommanData.Rows[0]["MQKeepAliveTime"]);
                // objLogger.fnWriteLog("Exit", "fnBindAllDatabasesAndTables", "Error");
            }
            catch (Exception ex)
            {
                objLogger.fnWriteLog(ex.Message + " ** " + ex.InnerException, "fnBindAllDatabasesAndTables", "Error");
            }
            objLogger.fnWriteLog("Exit", "fnBindAllDatabasesAndTables", "Info");
        }
    }
}
