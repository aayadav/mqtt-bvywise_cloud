﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using System.Windows.Forms;

namespace BvywiseMQTT
{
    public class BvywiseMQ
    {
        MqttClient objMQTTClient;
        clsCloudDataLayer objclsCloudDataLayer;
        clsPostgreSQLBasic clsPostgreSQLBasic;
        clsLogs objLogger;

        string strDeviceId = string.Empty;
        bool IsInternetConnection = false;
        bool blnStartCloudCall = true;

        DataTable dtGetAllData = new DataTable();
        DataTable dtGetProdCall = new DataTable();
        DataTable dtParamters = new DataTable();
        DataTable dtCommanData = new DataTable();


        string strDbMachineName = string.Empty;
        string strTblDeviceName = string.Empty;
        string strFeatureTblQuery = string.Empty;

        string strFeatureDeleteQuery = string.Empty;
        List<string> lstParameterColumnList = new List<string>();
        List<string> lstProductionColumnList = new List<string>();
        string strPalletChangeProgramName = string.Empty;

        int iTimeOut = 700;
        //string strIsPartCount = string.Empty;
        //int Devicecnt = 0;

        List<string> strlstParaList = new List<string>();
        string strMachinestatusTopic = string.Empty;
        string strParametersTopic = string.Empty;
        string strProductionTopic = string.Empty;
        string strAlarmsTopic = string.Empty;
        string strUserName = string.Empty;
        string strPassword = string.Empty;
        string strIP = string.Empty;
        string strMachineStatus = string.Empty;

        string strProductionQuery = string.Empty;
        string strAlarmTblQuery = string.Empty;
        string strDeleteProdQuery = string.Empty;
        string strDeleteAlarms = string.Empty;
        string strCheckIntenet = string.Empty;
        string DBCustomer = string.Empty;
        ushort ikeepAive = 5;
        string strCommunicationtype = string.Empty;
        string strConnctionString = string.Empty;

        int iParameterQOS = 0;
        int iProductionQOS = 0;

        //public BvywiseMQ(DataTable dtParameterData, string strLogFN, string strDeviceID, string strDeviceName, string strMachineName, int iDevcnt, string strCustName, string strbasicUrl, string strChkInternet, int timeout, string strPalletProgName, string PartCount, DataTable dtCmnData, string strPartProgramNo, string keepAlivetime)
        public BvywiseMQ(DataTable dtParameterData, string strLogFN, string strDeviceID, string strDeviceName, string strMachineName, int iDevcnt, string strCustName, string strbasicUrl, string strChkInternet, int timeout, string strPalletProgName, DataTable dtCmnData, string strComType, string keepAlivetime)
        {
            dtParamters = dtParameterData;
            objLogger = new clsLogs(strLogFN);
            strDbMachineName = strDeviceName;
            strTblDeviceName = strDeviceName;
            DBCustomer = strMachineName;
            strDeviceId = strDeviceID;
            //Devicecnt = iDevcnt;
            iTimeOut = timeout;
            //strIsPartCount = PartCount;
            dtCommanData = dtCmnData;
            strCheckIntenet = strChkInternet;
            //strPartNumber = strPartProgramNo;
            strCommunicationtype = strComType;
            ikeepAive = Convert.ToUInt16(keepAlivetime);
            //objLogger.fnWriteLog("IsPart Count " + strIsPartCount, "Constructor", "Debug");
            strConnctionString = strbasicUrl;
            strFeatureTblQuery = "Select * from '" + strTblDeviceName + "'";
            strPalletChangeProgramName = strPalletProgName;
            strFeatureDeleteQuery = "Delete from '" + strTblDeviceName + "'";

            strProductionQuery = "Select * from " + strTblDeviceName + "_ProdCount";
            strAlarmTblQuery = "Select * from " + strTblDeviceName + "_Alarms";
            strDeleteProdQuery = "Delete from " + strTblDeviceName + "_ProdCount";
            strDeleteAlarms = "Delete from " + strTblDeviceName + "_Alarms";

            objclsCloudDataLayer = new clsCloudDataLayer(strLogFN, strTblDeviceName);

            clsPostgreSQLBasic = new clsPostgreSQLBasic(strLogFN, strConnctionString, DBCustomer);

            //objLogger.fnWriteLog("Main DB Connection Sring= " + strConnctionString, "clsReadWritePostgreSQL", "Debug");

            //clsPostgreSQLBasic.fnCreatePgDatabase(strConnctionString, DBCustomer);

            lstParameterColumnList = clsPostgreSQLBasic.fnGetDeviceParaList(dtParamters);

            string strTblQuery = clsPostgreSQLBasic.fnCreateTableQuery(lstParameterColumnList, strTblDeviceName, strPalletChangeProgramName, strComType);
            objLogger.fnWriteLog("Create Table Query= " + strTblQuery, "clsReadWritePostgreSQL", "Debug");

            clsPostgreSQLBasic.fnCreateTables(strConnctionString, DBCustomer, strTblDeviceName, strTblQuery);

            lstProductionColumnList = clsPostgreSQLBasic.fnCreateProductionTable(strConnctionString, DBCustomer, strTblDeviceName);

            //if (!string.IsNullOrEmpty(strPalletChangeProgramName))
            //{
            // lstParameterColumnList = objclsCloudDataLayer.fnGetDeviceParaList_CreateTbl(DBCustomer, strDeviceName, dtParamters, strPalletChangeProgramName);
            //lstProductionColumnList = objclsCloudDataLayer.fnCreateProductionTable(strTblDeviceName, DBCustomer);
            //}
            iParameterQOS = Convert.ToInt32(dtCommanData.Rows[0]["ParameterQOS"]);
            iProductionQOS = Convert.ToInt32(dtCommanData.Rows[0]["ProductionQOS"]);
            fnAddParameterColumnList(dtParamters);
            strIP = Convert.ToString(dtCommanData.Rows[0]["MQHostname"]);
            objMQTTClient = new MqttClient(strIP);
        }

        public bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead("http://www.google.com/"))
                {
                    IsInternetConnection = true;
                    // return true;
                }
            }
            catch
            {
                IsInternetConnection = false;
            }
            return IsInternetConnection;
        }

        public bool PublishMessage(string strTopic, string message, int intid)
        {
            objLogger.fnWriteLog("Entry", "PublishMessage", "Info");
            bool flag = false;
            try
            {
                byte bt = 2;
                if (objMQTTClient.IsConnected)
                {
                    if (intid == 0) { bt = 0; }
                    else if (intid == 1) { bt = 1; }
                    else bt = 2;
                    ushort msgId = objMQTTClient.Publish(strTopic, // topic
                                        Encoding.UTF8.GetBytes(message), // message body
                                        bt, // QoS level
                                        true);
                    objLogger.fnWriteLog("QOS = " + bt + " Message Id & Topic = " + msgId + " ** " + strTopic, "PublishMessage", "Debug");
                    //if (msgId != 0)
                    flag = true;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                objLogger.fnWriteLog(ex.Message + "____" + ex.InnerException, "PublishMessage", "Error");
            }
            objLogger.fnWriteLog("Exit", "PublishMessage", "Info");
            return flag;
        }

        public bool fnConnect()
        {
            objLogger.fnWriteLog("Entry", "fnConnect", "Info");
            bool blnReturnFlag = false;
            while (!objMQTTClient.IsConnected)
            {
                try
                {
                    string strtopicstatus = Convert.ToString(dtCommanData.Rows[0]["MQMachineStatusTopic"]);
                    string stronlyProductionTopic = Convert.ToString(dtCommanData.Rows[0]["MQProductionTopic"]);

                    strMachinestatusTopic = strTblDeviceName + strtopicstatus;
                    strProductionTopic = strTblDeviceName + stronlyProductionTopic;
                    strIP = Convert.ToString(dtCommanData.Rows[0]["MQHostname"]);
                    strUserName = Convert.ToString(dtCommanData.Rows[0]["MQUsername"]);
                    strPassword = Convert.ToString(dtCommanData.Rows[0]["MQPassword"]);

                    objLogger.fnWriteLog("Topics " + strMachinestatusTopic + "_____" + strProductionTopic, "fnConnect", "Debug");

                    objMQTTClient = new MqttClient(strIP);//"192.168.225.147"//iphost.AddressList[2]//"192.168.0.100"

                    // register to message received
                    //client.MqttMsgPublished += client_MqttMsgPublished;
                    objMQTTClient.MqttMsgSubscribed += Client_MqttMsgSubscribed;
                    objMQTTClient.MqttMsgPublishReceived += Client_MqttMsgPublishReceived;
                    var clientId = Guid.NewGuid().ToString();
                    var val = objMQTTClient.Connect(strTblDeviceName, strUserName, strPassword, true, ikeepAive);//, true, 60);//, false, 60);//"techmetro2019"
                    if (objMQTTClient.IsConnected)
                        blnReturnFlag = true;
                    objLogger.fnWriteLog("Connection value= " + val + "Username & Password" + strUserName + " " + strPassword, "fnConnect", "Debug");
                }
                catch (Exception ex)
                {
                    objLogger.fnWriteLog("Exception= " + ex.Message, "fnConnect", "Error");
                }
                //keep alive plus 10 sec.
                int ikeepAliveT = (ikeepAive * 1000) + 10000;
                objLogger.fnWriteLog("Keep Alive Time= " + ikeepAliveT, "fnConnect", "Debug");
                Thread.Sleep(ikeepAliveT);
            }
            objLogger.fnWriteLog("Exit", "fnConnect", "Info");
            return blnReturnFlag;
        }

        public void fnCheckConnection()
        {
            objLogger.fnWriteLog("Entry", "fnCheckConnection", "Info");
            while (blnStartCloudCall)
            {
                Thread.Sleep(30000);
                try
                {
                    if (objMQTTClient.IsConnected)
                    {
                        objLogger.fnWriteLog("MQTTClient.IsConnected== " + objMQTTClient.IsConnected, "fnCheckConnection", "Debug");
                    }
                    else
                    {
                        objLogger.fnWriteLog("MQTTClient.IsConnected== " + objMQTTClient.IsConnected, "fnCheckConnection", "Debug");
                        fnConnect();
                    }
                    //Thread.Sleep(100);
                }
                catch (Exception ex)
                {
                    fnConnect();
                    objLogger.fnWriteLog(ex.Message + "____" + ex.InnerException, "fnCheckConnection", "Error");
                }
                objLogger.fnWriteLog("Exit", "fnCheckConnection", "Error");
            }
        }

        private void ObjMQTTClient_ConnectionClosed(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            //Thread.Sleep(1000);
            //string str = Encoding.UTF8.GetString(e.Message);
            //fnCreateQuery(Encoding.UTF8.GetString(e.Message));
        }

        private void Client_MqttMsgSubscribed(object sender, MqttMsgSubscribedEventArgs e)
        {

        }

        private void fnAddParameterColumnList(DataTable dt)
        {
            objLogger.fnWriteLog("Entry", "fnAddParameterColumnList", "Info");
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    strlstParaList.Add(Convert.ToString(dt.Rows[i]["ParameterName"]).Replace(" ", "_"));
                }
            }
            catch (Exception ex)
            {
                objLogger.fnWriteLog(ex.Message, "fnAddParameterColumnList", "Error");
            }
            objLogger.fnWriteLog("Exit", "fnAddParameterColumnList", "Info");
        }

        public bool blnpalletflag = true;
        //bool blnOneTimeFlag = true;
        int ipalletCount = 0;
        List<string> lststrProdResultData;
        string strPrvPartNo = string.Empty;
        string strProgramNM = string.Empty;
        public int _ipartCounter = 0;

        public void fnWritePalletChangeDataOrDAQToDB()
        {
            objLogger.fnWriteLog("Entry", "fnWritePalletChangeDataToCloud", "Info");
            string strProductionTblName = strTblDeviceName + "_ProdCount";
            try
            {
                //Read from DAQ
                objclsCloudDataLayer.fnDAQGetAllDetails(strTblDeviceName, strFeatureTblQuery, out dtGetAllData);
                int idtCount = dtGetAllData.Rows.Count;
                int idtColumnCount = dtGetAllData.Columns.Count;
                string[] arrResultData = new string[idtColumnCount];
                objLogger.fnWriteLog("Feature Data Row Count = " + idtCount, "fnWritePalletChangeDataOrDAQToDB", "Debug");
                if (idtCount > 0)
                {
                    //Delete data from DAQ
                    objclsCloudDataLayer.fnDAQInsertUpdateDeleteData(strTblDeviceName, strTblDeviceName, strFeatureDeleteQuery);
                    for (int i = (dtGetAllData.Rows.Count - 1); i < dtGetAllData.Rows.Count; i++)
                    {
                        #region 1st Send Activity Status

                        bool flag = fnSendFeatureData(dtGetAllData);
                        if (flag)
                        {
                            objLogger.fnWriteLog("Feature Data Flag= " + flag, "fnFeatureData", "Debug");
                        }
                        else
                        {
                            objLogger.fnWriteLog("Feature Data Flag= " + flag, "fnFeatureData", "Debug");
                        }
                        #endregion

                        for (int j = 1; j < idtColumnCount; j++)
                        {
                            arrResultData[j] = Convert.ToString(dtGetAllData.Rows[i][j]);
                        }
                        if (!string.IsNullOrEmpty(strPalletChangeProgramName))
                        {
                            //if (string.IsNullOrEmpty(strIsPartCount))
                            //{
                            strProgramNM = Convert.ToString(dtGetAllData.Rows[i]["Program_Number"]);
                            objLogger.fnWriteLog("ProgramName=" + strProgramNM, "fnWritePalletChangeDataToCloud", "Debug");
                            if (blnpalletflag)
                            {
                                if ((strProgramNM == strPalletChangeProgramName) && !string.IsNullOrEmpty(strPrvPartNo))
                                {
                                    ipalletCount += 1;
                                    blnpalletflag = false;
                                    string strAddedTime = string.Empty;
                                    //if (string.IsNullOrEmpty(strIsPartCount))
                                    {
                                        lststrProdResultData = new List<string>();
                                        lststrProdResultData.Add(strDeviceId);
                                        lststrProdResultData.Add(Convert.ToString(ipalletCount));
                                        lststrProdResultData.Add(strPrvPartNo);//strProgramNM
                                        string strQuery1 = objclsCloudDataLayer.fnInsertProdCntQuery(lstProductionColumnList, lststrProdResultData, strProductionTblName, out strAddedTime);

                                        bool proflag = _fnSendPallentChangeData(strPrvPartNo, strAddedTime);
                                        objLogger.fnWriteLog("PalletChange Send Flag= " + proflag, "fnWritePalletChangeDataToCloud", "Debug");

                                        bool flag1 = clsPostgreSQLBasic.fnInsertUpdateDelete(strQuery1);
                                        //bool flag1 = objclsCloudDataLayer.fnKloudQInsertUpdateDeleteData(DBCustomer, strTblDeviceName, strQuery1, dtParamters);
                                        objLogger.fnWriteLog("Production Insert Flag= " + flag1 + " Production Query=" + strQuery1 + "  Production Count and Prog Name= " + ipalletCount + " " + strPrvPartNo, "fnWritePalletChangeDataToCloud", "Debug");
                                        _ipartCounter = 0;
                                    }
                                    //else
                                    //{
                                    //objLogger.fnWriteLog("Production Count " + ipalletCount, "fnKloudQCallStart", "Debug");
                                    // }
                                }
                                else if (strProgramNM != "0" && strProgramNM != strPalletChangeProgramName)
                                {
                                    strPrvPartNo = strProgramNM;
                                }
                            }
                            else if (strProgramNM != strPalletChangeProgramName && strProgramNM != "0")
                            {
                                objLogger.fnWriteLog("Part Counter = " + _ipartCounter, "fnWritePalletChangeDataToCloud", "Debug");
                                if (_ipartCounter >= 20)
                                {
                                    strPrvPartNo = strProgramNM;
                                    blnpalletflag = true;
                                }
                                else
                                {
                                    _ipartCounter += 1;
                                }
                            }
                            //}
                            string strQuery = objclsCloudDataLayer.fnInsertQuery(arrResultData, strTblDeviceName, lstParameterColumnList, strPalletChangeProgramName, strCommunicationtype, Convert.ToString(ipalletCount));
                            //bool flag = objclsCloudDataLayer.fnKloudQInsertUpdateDeleteData(DBCustomer, strTblDeviceName, strQuery, dtParamters);
                            bool flag2 = clsPostgreSQLBasic.fnInsertUpdateDelete(strQuery);
                            objLogger.fnWriteLog("Insertion Flag= " + flag2 + " Featuredata insert query= " + strQuery, "fnWritePalletChangeDataToCloud", "Debug");
                        }
                        else
                        {
                            string strQuery = objclsCloudDataLayer.fnInsertQuery(arrResultData, strTblDeviceName, lstParameterColumnList, strPalletChangeProgramName, strCommunicationtype, Convert.ToString(ipalletCount));
                            bool flag2 = clsPostgreSQLBasic.fnInsertUpdateDelete(strQuery);
                            objLogger.fnWriteLog("Insertion Flag = " + flag2 + " Direct Featuredata insert query= " + strQuery, "fnWritePalletChangeDataToCloud", "Debug");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objLogger.fnWriteLog(ex.Message + " * " + ex.InnerException, "fnWritePalletChangeDataToCloud", "Error");
            }
            objLogger.fnWriteLog("Exit", "fnWritePalletChangeDataToCloud", "Info");
        }

        public void fnFeatureData()
        {
            objLogger.fnWriteLog("Entry", "fnFeatureData", "Info");
            int iTimeDiff = 0;
            //fnConnect();
            while (blnStartCloudCall)
            {
                DateTime dtactivity = DateTime.Now;
                if (strCheckIntenet == "Yes")
                {
                    CheckForInternetConnection();
                }
                else
                {
                    IsInternetConnection = true;
                }
                try
                {
                    if (IsInternetConnection)
                    {
                        fnWritePalletChangeDataOrDAQToDB();

                        fnOnlyProductionAndAlarm();
                        //clsPostgreSQLBasic.fnCloudGetAllDetails("Select * from " + strTblDeviceName, out dtGetAllData);

                        //if (!string.IsNullOrEmpty(strPalletChangeProgramName))
                        //{
                        //    fnWritePalletChangeDataOrDAQToDB();
                        //    objclsCloudDataLayer.fnCloudGetAllDetails(DBCustomer, strFeatureTblQuery, out dtGetAllData);
                        //    objLogger.fnWriteLog("DB Name= " + DBCustomer + " Feature Count " + dtGetAllData.Rows.Count, "fnFeatureData", "Debug");
                        //}
                        //else
                        //{
                        //    //Read from DAQ
                        //    objclsCloudDataLayer.fnDAQGetAllDetails(strDbMachineName, strFeatureTblQuery, out dtGetAllData);
                        //    objLogger.fnWriteLog("DB Name= " + strDbMachineName + " Feature Count " + dtGetAllData.Rows.Count, "fnFeatureData", "Debug");
                        //}

                        //if (dtGetAllData.Rows.Count > 0)
                        //{
                        //    //for (int i = (dtGetAllData.Rows.Count - 1); i < dtGetAllData.Rows.Count; i++)
                        //    {
                        //        //string[] arr= Convert.ToString(dtGetAllData.Rows[i]["AddedDateTime"]).Split('.');
                        //        //bool flag = fnSendFeatureData(dtGetAllData,Convert.ToInt64(arr[0]));
                        //        bool flag = fnSendFeatureData(dtGetAllData);

                        //        //insert into postgresql database 
                        //        if (flag)
                        //        {
                        //            objLogger.fnWriteLog("Feature Data Flag= " + flag, "fnFeatureData", "Debug");
                        //            //no need of if condition
                        //            //if (!string.IsNullOrEmpty(strPalletChangeProgramName))
                        //            //    objclsCloudDataLayer.fnCloudInsertUpdateDeleteData(DBCustomer, strTblDeviceName, strFeatureDeleteQuery);
                        //            //else
                        //            //Delete from DAQ
                        //            objclsCloudDataLayer.fnDAQInsertUpdateDeleteData(strDbMachineName, strTblDeviceName, strFeatureDeleteQuery);
                        //        }
                        //        else
                        //        {
                        //            objLogger.fnWriteLog("Feature Data Flag= " + flag, "fnFeatureData", "Debug");
                        //        }
                        //    }
                        //}
                    }
                    else
                    {
                        objLogger.fnWriteLog("No Internet Connection", "fnFeatureData", "Debug");
                    }
                }
                catch (Exception ex)
                {
                    objLogger.fnWriteLog(ex.Message + "  " + ex.InnerException, "fnFeatureData", "Error");
                }

                DateTime dtactivity2 = DateTime.Now;
                TimeSpan tspn = dtactivity2 - dtactivity;
                iTimeDiff = iTimeOut - Convert.ToInt32(tspn.TotalMilliseconds);
                if (iTimeDiff >= 0)
                {
                    objLogger.fnWriteLog("Wait time when FeatureData=  " + iTimeDiff, "fnFeatureData", "Debug");
                    Thread.Sleep(iTimeDiff);
                }
            }
            objLogger.fnWriteLog("Exit", "fnFeatureData", "Info");
        }

        string strStatus = string.Empty;
        string strTimestamp = string.Empty;
        //long lPrevActivityEpochTime = 0;
        //int iActivityTimeSec = 0;
        //long iActivityTimeintmSec = 0;
        private bool fnSendFeatureData(DataTable dtAllDetails)
        {
            bool flag = false;
            string strNewEpochTime = string.Empty;
            objLogger.fnWriteLog("Entry", "fnSendFeatureData", "Info");
            try
            {
                //if (lnewEprchTime != lPrevActivityEpochTime)
                {
                    for (int i = (dtAllDetails.Rows.Count - 1); i < dtAllDetails.Rows.Count; i++)
                    {
                        if (Convert.ToString(dtAllDetails.Rows[i]["ActivityStatus"]) == "Producing")
                        {
                            strStatus = "1";
                        }
                        else if (Convert.ToString(dtAllDetails.Rows[i]["ActivityStatus"]) == "Idle" || Convert.ToString(dtAllDetails.Rows[i]["ActivityStatus"]) == "Stop")
                        {
                            strStatus = "0";
                        }
                        else
                        {
                            strStatus = "4";
                        }
                        string[] strEpochtimearr = Convert.ToString(dtAllDetails.Rows[i]["AddedDateTime"]).Split('.');
                        //objLogger.fnWriteLog("Timestamp in Sec=" + Convert.ToString(strEpochtimearr[0]), "fnSendProductionData", "Error");
                        //lPrevActivityEpochTime = Convert.ToInt64(strEpochtimearr[0]);
                        strTimestamp = Convert.ToString(Convert.ToInt64(strEpochtimearr[0]) * 1000);
                        //strTimestamp = Convert.ToString(dtAllDetails.Rows[i]["AddedDateTime"]);
                        strMachineStatus = "{\"timestamp\":" + strTimestamp + ",\"machine_status\":" + strStatus + "}";
                        //strMachineStatus = "{\"machine_status\":" + strStatus + "}";
                        objLogger.fnWriteLog("Machine Status String=" + strMachineStatus, "fnSendFeatureData", "Debug");

                        if (PublishMessage(strMachinestatusTopic, strMachineStatus, iParameterQOS))
                        {
                            flag = true;
                            objLogger.fnWriteLog("Machine status send flag= " + flag, "fnSendFeatureData", "Debug");
                        }
                        else
                        {
                            flag = false;
                            objLogger.fnWriteLog("Machine status send flag= " + flag, "fnSendFeatureData", "Debug");
                        }
                    }
                }
                //else
                //{
                //    flag = true;
                //    objLogger.fnWriteLog("Feature Data Already send", "fnSendFeatureData", "Debug");
                //}
            }
            catch (Exception ex)
            {
                objLogger.fnWriteLog(ex.Message + "  " + ex.InnerException, "fnSendFeatureData", "Error");
            }
            objLogger.fnWriteLog("Exit", "fnSendFeatureData", "Info");
            return flag;
        }

        public void fnThreadProductionAndAlarm()
        {
            objLogger.fnWriteLog("Entry", "fnThreadProductionAndAlarm", "Info");
            int iTimeDiff = 0;
            string strProductionTblName = strTblDeviceName + "_ProdCount";

            while (blnStartCloudCall)
            {
                //Thread.Sleep(1000);
                try
                {
                    DateTime dtactivity = DateTime.Now;
                    //CheckForInternetConnection();
                    if (IsInternetConnection)
                    {
                        //for (int i = 0; i < Devicecnt; i++)
                        {
                            //dtProStart = DateTime.Now;
                            if (!string.IsNullOrEmpty(strPalletChangeProgramName))
                            {
                                clsPostgreSQLBasic.fnCloudGetAllDetails("Select * from " + strTblDeviceName + "_ProdCount", out dtGetProdCall);
                                //objLogger.fnWriteLog("Production DB Name= " + DBCustomer + " Production Count " + dtGetProdCall.Rows.Count, "fnThreadProductionAndAlarm", "Debug");
                            }
                            else
                            {
                                objclsCloudDataLayer.fnDAQGetAllDetails(strDbMachineName, strProductionQuery, out dtGetProdCall);
                                //objLogger.fnWriteLog("Sqlite Production DB Name= " + strDbMachineName + " Production Count " + dtGetProdCall.Rows.Count, "fnThreadProductionAndAlarm", "Debug");
                            }
                            if (dtGetProdCall.Rows.Count > 0)
                            {
                                objLogger.fnWriteLog("Total Rows = " + dtGetProdCall.Rows.Count, "Production", "Debug");

                                objclsCloudDataLayer.fnDAQInsertUpdateDeleteData(strDbMachineName, strTblDeviceName, strDeleteProdQuery);
                                //if (objMQTTClient.IsConnected)
                                {
                                    for (int i = (dtGetProdCall.Rows.Count - 1); i < dtGetProdCall.Rows.Count; i++)
                                    {
                                        if (fnSendProductionData(dtGetProdCall, Convert.ToInt32(dtGetProdCall.Rows[i]["PartCount"])))
                                        {
                                            // if (!string.IsNullOrEmpty(strPalletChangeProgramName)) { }
                                            //objclsCloudDataLayer.fnCloudInsertUpdateDeleteData(DBCustomer, strTblDeviceName, strDeleteProdQuery);
                                            if (string.IsNullOrEmpty(strPalletChangeProgramName))
                                            {
                                                string strAddedTime = string.Empty;
                                                //for (int i = (dtGetProdCall.Rows.Count - 1); i < dtGetProdCall.Rows.Count; i++)
                                                //{
                                                lststrProdResultData = new List<string>();
                                                lststrProdResultData.Add(strDeviceId);
                                                lststrProdResultData.Add(Convert.ToString(dtGetProdCall.Rows[i]["PartCount"]));
                                                lststrProdResultData.Add(Convert.ToString(dtGetProdCall.Rows[i]["ProgramName"]));
                                                string strQuery1 = objclsCloudDataLayer.fnInsertProdCntQuery(lstProductionColumnList, lststrProdResultData, strProductionTblName, out strAddedTime);
                                                bool flag = clsPostgreSQLBasic.fnInsertUpdateDelete(strQuery1);
                                                if (flag)
                                                {
                                                    objLogger.fnWriteLog("Production Insert Flag = " + flag, "fnThreadProductionAndAlarm", "Debug");
                                                    //Delete DAQ Production Table Data
                                                }
                                                else
                                                {
                                                    objLogger.fnWriteLog("Production Insert Flag = " + flag, "fnThreadProductionAndAlarm", "Debug");
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        objLogger.fnWriteLog("No Internet Connection", "fnThreadProductionAndAlarm", "Debug");
                    }
                    DateTime dtactivity2 = DateTime.Now;
                    TimeSpan tspn = dtactivity2 - dtactivity;
                    iTimeDiff = iTimeOut - Convert.ToInt32(tspn.TotalMilliseconds);
                    if (iTimeDiff >= 0)
                    {
                        //objLogger.fnWriteLog("Wait time when ProductionAndAlarm= " + iTimeDiff, "fnThreadProductionAndAlarm", "Debug");
                        Thread.Sleep(iTimeDiff);
                    }
                }
                catch (Exception ex)
                {
                    objLogger.fnWriteLog(ex.Message + "  " + ex.InnerException, "fnThreadProductionAndAlarm", "Error");
                }
            }
            objLogger.fnWriteLog("Exit", "fnThreadProductionAndAlarm", "Info");
        }

        public void fnOnlyProductionAndAlarm()
        {
            objLogger.fnWriteLog("Entry", "fnOnlyProductionAndAlarm", "Info");
            string strProductionTblName = strTblDeviceName + "_ProdCount";
            try
            {

                if (IsInternetConnection)
                {
                    objclsCloudDataLayer.fnDAQGetAllDetails(strDbMachineName, strProductionQuery, out dtGetProdCall);
                    if (dtGetProdCall.Rows.Count > 0)
                    {
                        objLogger.fnWriteLog("Total Rows = " + dtGetProdCall.Rows.Count, "fnOnlyProductionAndAlarm", "Debug");

                        objclsCloudDataLayer.fnDAQInsertUpdateDeleteData(strDbMachineName, strTblDeviceName, strDeleteProdQuery);

                        for (int i = (dtGetProdCall.Rows.Count - 1); i < dtGetProdCall.Rows.Count; i++)
                        {
                            if (fnSendProductionData(dtGetProdCall, Convert.ToInt32(dtGetProdCall.Rows[i]["PartCount"])))
                            {
                                string strAddedTime = string.Empty;
                                lststrProdResultData = new List<string>();
                                lststrProdResultData.Add(strDeviceId);
                                lststrProdResultData.Add(Convert.ToString(dtGetProdCall.Rows[i]["PartCount"]));
                                lststrProdResultData.Add(Convert.ToString(dtGetProdCall.Rows[i]["ProgramName"]));
                                string strQuery1 = objclsCloudDataLayer.fnInsertProdCntQuery(lstProductionColumnList, lststrProdResultData, strProductionTblName, out strAddedTime);
                                bool flag = clsPostgreSQLBasic.fnInsertUpdateDelete(strQuery1);
                                if (flag)
                                {
                                    objLogger.fnWriteLog("Production Insert Flag = " + flag, "fnOnlyProductionAndAlarm", "Debug");
                                    //Delete DAQ Production Table Data
                                }
                                else
                                {
                                    objLogger.fnWriteLog("Production Insert Flag = " + flag, "fnOnlyProductionAndAlarm", "Debug");
                                }
                            }
                        }
                    }
                    else
                    {
                        objLogger.fnWriteLog("No Production Data Found", "fnOnlyProductionAndAlarm", "Debug");
                    }
                }
                else
                {
                    objLogger.fnWriteLog("No Internet Connection", "fnOnlyProductionAndAlarm", "Debug");
                }
            }
            catch (Exception ex)
            {
                objLogger.fnWriteLog(ex.Message + "  " + ex.InnerException, "fnOnlyProductionAndAlarm", "Error");
            }
            objLogger.fnWriteLog("Exit", "fnOnlyProductionAndAlarm", "Info");
        }

        string strProgramName = string.Empty;
        string strPartCount = string.Empty;
        string strProdTimestamp = string.Empty;
        string strProductuonString = string.Empty;
        string strAlarmString = string.Empty;
        string strPartNumber = string.Empty;
        int iprevpartcount = 0;
        //int iEpochTimeinsec = 0;
        // long imSec = 0;
        public bool fnSendProductionData(DataTable dtProdCall, int inewParcount)
        {
            objLogger.fnWriteLog("Entry", "fnSendProductionData", "Info");
            bool blnFlag = false;
            try
            {
                for (int i = (dtProdCall.Rows.Count - 1); i < dtProdCall.Rows.Count; i++)
                //for (int i = 0; i < dtProdCall.Rows.Count; i++)
                {
                    if (iprevpartcount != inewParcount)
                    {
                        // strPartNumber = "123456789";
                        strProgramName = "12345";//Convert.ToString(dtProdCall.Rows[i]["ProgramName"]);
                        strPartNumber = Convert.ToString(dtProdCall.Rows[i]["ProgramName"]);
                        strPartCount = "1";
                        iprevpartcount = Convert.ToInt32(dtProdCall.Rows[i]["PartCount"]);
                        string[] strEpochtimearr = Convert.ToString(dtProdCall.Rows[i]["AddedDateTime"]).Split('.');
                        //iEpochTimeinsec = Convert.ToInt32(strEpochtimearr[0]);
                        //objLogger.fnWriteLog("Timestamp in Sec=" + (strEpochtimearr[0]), "fnSendProductionData", "Error");
                        // imSec = iEpochTimeinsec * 1000;
                        //strProdTimestamp = Convert.ToString(dtProdCall.Rows[i]["AddedDateTime"]);
                        strProdTimestamp = Convert.ToString(Convert.ToInt64(strEpochtimearr[0]) * 1000);
                        // strProductuonString = "{\"timestamp\":" + strProdTimestamp + ",\"part_number\":\"" + strPartNumber + "\",\"part_name\":" + strPartNumber + ",\"part_count\":" + strPartCount + "}";
                        strProductuonString = "{\"timestamp\":" + strProdTimestamp + ",\"part_number\":\"" + strPartNumber + "\",\"part_count\":" + strPartCount + "}";

                        //strProductuonString = "{\"part_number\":\"" + strPartNumber + "\",\"part_count\":" + strPartCount + "}";
                        objLogger.fnWriteLog("Production String and DB PartCount =" + strProductuonString + " ### " + Convert.ToString(dtProdCall.Rows[i]["PartCount"]), "fnSendProductionData", "Debug");

                        if (PublishMessage(strProductionTopic, strProductuonString, iProductionQOS))
                        {
                            blnFlag = true;
                            objLogger.fnWriteLog("Production Data Sent flag true", "fnSendProductionData", "Debug");
                        }
                        else
                        {
                            blnFlag = false;
                            objLogger.fnWriteLog("Production Data Sent flag false", "fnSendProductionData", "Debug");
                        }
                    }
                    else
                    {
                        blnFlag = true;
                        objLogger.fnWriteLog("Prev part cnt= " + iprevpartcount + " and New partcount=" + inewParcount, "fnSendProductionData", "Debug");
                    }
                }
            }
            catch (Exception ex)
            {
                objLogger.fnWriteLog(ex.Message + "   " + ex.InnerException, "fnSendProductionData", "Error");
            }
            objLogger.fnWriteLog("Exit", "fnSendProductionData", "Info");
            return blnFlag;
        }

        public bool _fnSendPallentChangeData(string strProgramName, string strAddedTime)
        {
            bool blnFlag = false;
            string strPartCount = "1";
            try
            {
                string[] strEpochtimearr = Convert.ToString(strAddedTime).Split('.');
                string strProdTimestamp = Convert.ToString(Convert.ToInt64(strEpochtimearr[0]) * 1000);
                string strProductuonString = "{\"timestamp\":" + strProdTimestamp + ",\"part_number\":\"" + strProgramName + "\",\"part_count\":" + strPartCount + "}";
                if (PublishMessage(strProductionTopic, strProductuonString, iProductionQOS))
                {
                    blnFlag = true;
                    objLogger.fnWriteLog("Production Data Sent flag true", "fnSendProductionData", "Debug");
                }
                else
                {
                    blnFlag = false;
                    objLogger.fnWriteLog("Production Data Sent flag false", "fnSendProductionData", "Debug");
                }
            }
            catch (Exception ex)
            {
                blnFlag = false;
                objLogger.fnWriteLog(ex.Message + "   " + ex.InnerException, "_fnSendPallentChangeData", "Error");
            }
            return blnFlag;
        }

        public void fnSendAlarmURL(DataTable dtAlarms)
        {
            objLogger.fnWriteLog("Entry", "fnSendAlarmURL", "Info");
            try
            {
                for (int i = 0; i < dtAlarms.Rows.Count; i++)
                {
                    strAlarmString = "{\"macid\":" + strDeviceId + ",\"Alarm\":" + dtAlarms.Rows[i]["AlarmMessage"] + ",\"timestamp\": " + dtAlarms.Rows[i]["AddedDateTime"] + "}";
                    objLogger.fnWriteLog("Alarm String=" + strAlarmString, "fnSendAlarmURL", "Debug");
                    PublishMessage(strAlarmsTopic, strAlarmString, iParameterQOS);
                }
            }
            catch (Exception ex)
            {
                objLogger.fnWriteLog(ex.Message + "   " + ex.InnerException, "fnSendAlarmURL", "Error");
            }
            objLogger.fnWriteLog("Exit", "fnSendAlarmURL", "Info");
        }

        static DataTable ConvertListToDataTable(List<string[]> list)
        {
            // New table.
            DataTable table = new DataTable();

            // Get max columns.
            int columns = 0;
            foreach (var array in list)
            {
                if (array.Length > columns)
                {
                    columns = array.Length;
                }
            }

            // Add columns.
            for (int i = 0; i < columns; i++)
            {
                table.Columns.Add();
            }

            // Add rows.
            foreach (var array in list)
            {
                table.Rows.Add(array);
            }
            return table;
        }

    }
}
