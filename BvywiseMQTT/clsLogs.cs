﻿//using PLCCommDAQ;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace BvywiseMQTT
{
    public class clsLogs
    {
        public string strFileName = string.Empty;
        string strNewNo = string.Empty;
        public clsLogs(string strFName)
        {
            strFileName = strFName;
        }

        public void fnWriteLog(string strLogMsg,string strfnName,string strLogType)
        {
            try
            {
                if(!File.Exists(strFileName))
                {
                    var myFile = File.Create(strFileName);
                    myFile.Close();
                }
                FileInfo fi = new FileInfo(strFileName);

                if (fi.Length > 5000000)
                {
                    string strTempFile = Path.GetFileNameWithoutExtension(strFileName);

                    char stringLastCharacter2 = strTempFile.ToCharArray().ElementAt(strTempFile.Length - 1);


                    if (char.IsNumber(stringLastCharacter2))
                    {
                        switch (stringLastCharacter2.ToString())
                        {
                            case "0":
                                strNewNo = "1";
                                break;
                            case "1":
                                strNewNo = "2";
                                break;
                            case "2":
                                strNewNo = "3";
                                break;
                            case "3":
                                strNewNo = "4";
                                break;
                            case "4":
                                strNewNo = "0";
                                break;                          
                            default:
                                strNewNo = "0";
                                break;
                        }
                    }
                    else
                    {
                        strNewNo = "0";
                    }
                    strFileName = Application.StartupPath + "\\Logs\\" + strTempFile.Substring(0, strTempFile.Length - 1) + strNewNo + ".log";
                    fnRecreateFile();

                }
                using (StreamWriter writetext = File.AppendText(strFileName))
                {
                  if (clsCloudDataLayer.strLogType == strLogType || strLogType == "Error")
                    {
                        //2017 - 12 - 15 17:09:35, Error, fnExecuteURL, Create Device Response: OK
                        writetext.WriteLine("" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss:fff") + ", " + strLogType + ", " + strfnName + ", " + strLogMsg);
                       writetext.Close();
                    }
                }
            }
            catch(Exception ex)
            {
                string s = ex.Message;
               // fnWriteLog(ex.Message, "fnWriteLog", "Error");
            }
        }

        private void fnRecreateFile()
        {
            try
            {
                if (File.Exists(strFileName))
                {
                    File.Delete(strFileName);
                    Thread.Sleep(1);
                    var myFile = File.Create(strFileName);
                    myFile.Close();
                }
            }
            catch(FileNotFoundException fex)
            {
                string s = fex.Message;// fnWriteLog(fex.Message, "fnRecreateFile", "Error");
            }
            catch(Exception ex)
            {
              string s = ex.Message;//fnWriteLog(ex.Message, "fnRecreateFile", "Error");
            }
        }
    }
}
