﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BvywiseMQTT
{
    class clsCloudDataLayer
    {
        public static string strLogType = "Error";

        clsLogs objclsLogger;
        SQLiteConnection DAQConnection;
        //SQLiteConnection CloudConnection;
        //SQLiteCommand mqCmd;
        //SQLiteDataAdapter mqDA;
        public clsCloudDataLayer(string strLogFilename)
        {
            //objclsLogger = new clsLogs(strLogFilename);
        }
        // public clsCloudDataLayer(string strLogFilename, string strDaqDatabase, string strCloudDatabse)

        public clsCloudDataLayer(string strLogFilename, string strDaqDatabase)
        {
            //objclsLogger = new clsLogs(Application.StartupPath + "\\" + strLogFilename + "_BvywiseCloudCall" + "_0.log");
            objclsLogger = new clsLogs(strLogFilename);

            DAQConnection = new SQLiteConnection("Data Source=" + Application.StartupPath + "\\" + strDaqDatabase + ".db" + ";Version=3;Synchronous=Off;Temp Store=Memory;Journal Mode=WAL;Locking Mode=Exclusive;Page Size=4096;");

            // CloudConnection = new SQLiteConnection("Data Source=" + Application.StartupPath + "\\" + strCloudDatabse + ".db" + ";Version=3;Synchronous=Off;Temp Store=Memory;Journal Mode=WAL;Locking Mode=Exclusive;Page Size=4096;");
        }
        #region Call from Cloud Mediator

        public bool BindDataGridDetails(out DataTable dt, string strdbname, string query)
        {
            bool flag = false;
            //objclsLogger.fnWriteLog("Entry", "BindDataGridDetails", "Info");
            dt = new DataTable();
            try
            {
                //using (SQLiteConnection mqCon = new SQLiteConnection("Data Source=" + Application.StartupPath + "\\" + strdbname + ".db" + ";Version=3;"))
                using (SQLiteConnection mqCon = new SQLiteConnection("Data Source=" + Application.StartupPath + "\\" + strdbname + ".db" + ";Version=3;Synchronous=Off;Temp Store=Memory;Journal Mode=WAL;Locking Mode=Exclusive;Page Size=4096;Password=techm@16;"))
                {
                    mqCon.Open();
                    using (SQLiteDataAdapter mqDA = new SQLiteDataAdapter(query, mqCon))
                    {
                        mqDA.Fill(dt);
                        flag = true;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                string str = ex.Message + "__" + ex.InnerException;
                //objclsLogger.fnWriteLog(str, "BindDataGridDetails", "Error");
            }
            //objclsLogger.fnWriteLog("Exit", "BindDataGridDetails", "Info");
            return flag;
        }

        #endregion

        string date = string.Empty;

        public string fnInsertQuery(string[] strarrResultData, string tablename, List<string> lststrparalist, string strIsPalletCnt, string strCommtype, string strPartCount)
        {
            objclsLogger.fnWriteLog("Entry", "fnInsertQuery", "Info");
            string strQuery = string.Empty;
            string strValues = string.Empty;
            try
            {
                strValues = "Values(";
                strQuery = "Insert into " + tablename + "(";
                int iarrdatalen = strarrResultData.Length;
                int iLength = lststrparalist.Count;//
                for (int i = 0; i < iLength; i++)
                {
                    if (i > 0)
                    {
                        strQuery += ',';
                        strValues += ',';
                    }
                    strQuery += lststrparalist[i];
                    strValues += "'" + strarrResultData[i + 1] + "'";
                    //  strValues += ")";
                }
                if (strCommtype == "Read")
                {
                    if (!string.IsNullOrEmpty(strIsPalletCnt))
                    {
                        strValues += "," + "'" + strarrResultData[iarrdatalen - 2] + "'" + "," + "'" + strarrResultData[iarrdatalen - 1] + "'" + "," + "'" + strPartCount + "'" + ")";
                        strQuery += ",AddedDateTime,ActivityStatus,PalletChangeCount)" + strValues;
                    }
                    else
                    {
                        strValues += "," + "'" + strarrResultData[iarrdatalen - 2] + "'" + "," + "'" + strarrResultData[iarrdatalen - 1] + "'" + ")";
                        strQuery += ",AddedDateTime,ActivityStatus)" + strValues;
                    }

                }
                else
                {
                    if (!string.IsNullOrEmpty(strIsPalletCnt))
                    {
                        strValues += "," + "'" + strarrResultData[iarrdatalen - 5] + "'" + "," + "'" + strarrResultData[iarrdatalen - 4] + "'" + "," + "'" + strarrResultData[iarrdatalen - 3] + "'" + "," + "'" + strarrResultData[iarrdatalen - 2] + "'" + "," + "'" + strarrResultData[iarrdatalen - 1] + "'" + ")";
                        strQuery += ",ProducingTime,IdelTime,AddedDateTime,ActivityStatus,PlcState)" + strValues;
                    }
                    else
                    {
                        strValues += "," + "'" + strarrResultData[iarrdatalen - 5] + "'" + "," + "'" + strarrResultData[iarrdatalen - 4] + "'" + "," + "'" + strarrResultData[iarrdatalen - 3] + "'" + "," + "'" + strarrResultData[iarrdatalen - 2] + "'" + "," + "'" + string.Empty + "'" + "," + "'" + strPartCount + "'" + ")";
                        strQuery += ",ProducingTime,IdelTime,AddedDateTime,ActivityStatus,PlcState,PalletChangeCount)" + strValues;
                    }
                }
            }
            catch (Exception ex)
            {
                objclsLogger.fnWriteLog(ex.Message + " * " + ex.InnerException, "fnInsertQuery", "Error");
            }
            objclsLogger.fnWriteLog("Exit", "fnInsertQuery", "Info");
            return strQuery;
        }


        SQLiteDataAdapter CloudDataAdapter;
        SQLiteCommand CloudCmd;

        public void fnDAQGetAllDetails(string strdbname, string strQuery, out DataTable dtResultData)
        {
            objclsLogger.fnWriteLog("Entry", "fnDAQGetAllDetails", "Info");
            dtResultData = new DataTable();
            try
            {
                if (DAQConnection.State != ConnectionState.Open)
                {
                    DAQConnection.Open();
                }
                CloudDataAdapter = new SQLiteDataAdapter(strQuery, DAQConnection);
                CloudDataAdapter.Fill(dtResultData);

            }
            catch (Exception ex)
            {
                objclsLogger.fnWriteLog(ex.Message + "###" + ex.InnerException, "fnDAQGetAllDetails", "Error");
            }
            objclsLogger.fnWriteLog("Exit", "fnDAQGetAllDetails", "Info");
        }

        public bool fnDAQInsertUpdateDeleteData(string strdbname, string strtblName, string strQuery)
        {
            objclsLogger.fnWriteLog("Entry", "fnDAQInsertUpdateDeleteData", "Info");
            bool flag = false;
            try
            {

                if (DAQConnection.State != ConnectionState.Open)
                {
                    DAQConnection.Open();
                }
                //SQLiteCommand CloudCmd = new SQLiteCommand(strQuery, CloudCon);
                using (SQLiteTransaction transaction = DAQConnection.BeginTransaction())
                {
                    CloudCmd = new SQLiteCommand(strQuery, DAQConnection);
                    CloudCmd.ExecuteNonQuery();
                    flag = true;
                    CloudCmd.Dispose();
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                DAQConnection.Close();
                objclsLogger.fnWriteLog(ex.Message + "###" + ex.InnerException, "fnDAQInsertUpdateDeleteData", "Error");
            }
            objclsLogger.fnWriteLog("Exit", "fnDAQInsertUpdateDeleteData", "Info");
            return flag;
        }

        public string fngetTimeSpan(bool isnet)
        {
            TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
            int secondsSinceEpoch = (int)t.TotalSeconds;
            return secondsSinceEpoch.ToString() + ".00";
        }

        public string fnInsertProdCntQuery(List<string> lststrProdCols, List<string> lststrResultData, string tablename, out string straddedtime)
        {
            string strQuery = string.Empty;
            string strValues = string.Empty;
            straddedtime = string.Empty;
            try
            {
                objclsLogger.fnWriteLog("Entry", "fnInsertProdCntQuery", "Info");
                // date = fngetTimeSpan(fnCheckInternet()); //[PPM] 06.05.2019 Hide as we are not using internet for getting timestamp
                straddedtime = fngetTimeSpan(true);
                strValues = "Values(";
                strQuery = "Insert into " + tablename + "(";
                for (int i = 0; i < lststrProdCols.Count; i++)
                {
                    if (i > 0)
                    {
                        strQuery += ',';
                        strValues += ',';
                    }
                    strQuery += lststrProdCols[i];
                    strValues += "'" + lststrResultData[i] + "'";
                    //  strValues += ")";
                }
                // strQuery += ",AddedDateTime)";
                strValues += "," + straddedtime + ")";
                strQuery += ",AddedDateTime)" + strValues;
                // objLogger.fnWriteLog("Exit", "fnInsertProdCntQuery", "Error");
            }
            catch (Exception ex)
            {
                objclsLogger.fnWriteLog(ex.Message, "fnInsertProdCntQuery", "Error");
            }
            objclsLogger.fnWriteLog("Exit", "fnInsertProdCntQuery", "Info");
            return strQuery;
        }

        #region hide no ref for this methods AAY 03.04.2020
        //string strdatabase = string.Empty;

        ////public void fnCreateTable(string Dbname, string strtblName, DataTable dt, string strIsPartCount)
        //public void fnCreateTable(string Dbname, string strtblName, DataTable dt, string strIsPallletChangeCount)
        //{
        //    objclsLogger.fnWriteLog("Entry", "fnCreateTable", "Info");
        //    try
        //    {
        //        string database = Dbname + ".db";
        //        objclsLogger.fnWriteLog("Create Table DB Name=" + database, "fnCreateTable", "Debug");
        //        SQLiteCommand command;
        //        // using (SQLiteConnection con = new SQLiteConnection("Data Source=" + Application.StartupPath + "\\" + database + ";Version=3;"))
        //        using (SQLiteConnection con = new SQLiteConnection("Data Source=" + Application.StartupPath + "\\" + database + ";Version=3;Synchronous=Off;Temp Store=Memory;Journal Mode=WAL;Locking Mode=Exclusive;Page Size=4096;"))
        //        {
        //            using (command = con.CreateCommand())
        //            {
        //                con.Open();
        //                command.CommandText = "CREATE TABLE IF NOT EXISTS " + " " + strtblName + "(";
        //                bool first = true;
        //                for (int i = 0; i < dt.Rows.Count; i++)
        //                {
        //                    if (!first) command.CommandText += ",";
        //                    else
        //                    {
        //                        first = false;
        //                        command.CommandText += "RowId" + " " + "INTEGER PRIMARY KEY AUTOINCREMENT,";
        //                    }
        //                    command.CommandText += Convert.ToString(dt.Rows[i]["ParameterName"]).Replace(" ", "_") + " " + "VARCHAR(100)";
        //                    command.Parameters.Add(new SQLiteParameter(Convert.ToString(dt.Rows[i]["ParameterName"]).Replace(" ", "_") + "VARCHAR(100)"));
        //                    //lststrParaList.Add(Convert.ToString(dt.Rows[i]["ParameterName"]).Replace(" ", "_"));
        //                }
        //                command.CommandText += ",";
        //                command.CommandText += "ProducingTime VARCHAR(100)";
        //                command.CommandText += ",IdelTime VARCHAR(100)";
        //                command.CommandText += ",AddedDateTime VARCHAR(100)";
        //                command.CommandText += ",ActivityStatus VARCHAR(100)";

        //                command.CommandText += ",PlcState VARCHAR(50)";
        //                if (!string.IsNullOrEmpty(strIsPallletChangeCount))
        //                {
        //                    command.CommandText += ",PalletChangeCount VARCHAR(50)";
        //                }
        //                command.CommandText += ");";
        //                command.ExecuteNonQuery();
        //                dt = new DataTable();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objclsLogger.fnWriteLog(ex.Message + "####" + ex.InnerException, "fnCreateTable", "Error");
        //    }
        //    objclsLogger.fnWriteLog("Exit", "fnCreateTable", "Info");
        //}

        //List<string> lststrProdCntCol = new List<string>();

        //public List<string> fnCreateProductionTable(string strTableName, string strdbName)//string strDatabaseName,
        //{
        //    objclsLogger.fnWriteLog("Entry", "fnCreateProductionTable", "Info");
        //    string strtblnmProduction = string.Empty;
        //    try
        //    {
        //        lststrProdCntCol.Clear();
        //        lststrProdCntCol.Add("DeviceId");
        //        lststrProdCntCol.Add("PartCount");
        //        lststrProdCntCol.Add("ProgramName");
        //        strtblnmProduction = strTableName + "_ProdCount";
        //        fnCreateProductionCountTable(strdbName, strtblnmProduction, lststrProdCntCol);
        //    }
        //    catch (Exception ex)
        //    {
        //        objclsLogger.fnWriteLog(ex.Message + "####" + ex.InnerException, "fnCreateProductionTable", "Error");
        //    }
        //    objclsLogger.fnWriteLog("Exit", "fnCreateProductionTable", "Info");
        //    return lststrProdCntCol;
        //}

        //public void fnCreateProductionCountTable(string strdbname, string strtablename, List<string> lstProdcntCols)
        //{
        //    objclsLogger.fnWriteLog("Entry", "fnCreateProductionCountTable", "Info");
        //    try
        //    {
        //        string database = strdbname + ".db";
        //        objclsLogger.fnWriteLog("Create Production Count TBL=" + database, "fnCreateProductionCountTable", "Error");
        //        SQLiteCommand command;
        //        //using (SQLiteConnection con = new SQLiteConnection("Data Source=" + Application.StartupPath + "\\" + database + ";Version=3;"))
        //        using (SQLiteConnection con = new SQLiteConnection("Data Source=" + Application.StartupPath + "\\" + database + ";Version=3;Synchronous=Off;Temp Store=Memory;Journal Mode=WAL;Locking Mode=Exclusive;Page Size=4096;"))
        //        {
        //            using (command = con.CreateCommand())
        //            {
        //                con.Open();
        //                command.CommandText = "CREATE TABLE IF NOT EXISTS " + " " + strtablename + "(";
        //                bool first = true;
        //                for (int i = 0; i < lstProdcntCols.Count; i++)
        //                {
        //                    if (!first) command.CommandText += ",";

        //                    else
        //                    {
        //                        first = false;
        //                        command.CommandText += "RowId" + " " + "INTEGER PRIMARY KEY AUTOINCREMENT,";
        //                    }
        //                    command.CommandText += Convert.ToString(lstProdcntCols[i].Replace(" ", "_")) + " " + "VARCHAR(100)";
        //                    command.Parameters.Add(new SQLiteParameter(Convert.ToString(lstProdcntCols[i].Replace(" ", "_")) + "VARCHAR(100)"));
        //                    //lststrParaList.Add(Convert.ToString(dt.Rows[i]["ParameterName"]).Replace(" ", "_"));
        //                }
        //                command.CommandText += ",";
        //                command.CommandText += "AddedDateTime VARCHAR(100)";
        //                command.CommandText += ");";
        //                command.ExecuteNonQuery();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objclsLogger.fnWriteLog(ex.Message + "###" + ex.InnerException, "fnCreateProductionCountTable", "Error");
        //    }
        //    objclsLogger.fnWriteLog("Exit", "fnCreateProductionCountTable", "Info");
        //}
        //List<string> lstParaColumnNm;

        //public List<string> fnGetDeviceParaList_CreateTbl(string dbname, string strDevNm, DataTable dtParamterData, string strIsPartCnt)
        //{
        //    objclsLogger.fnWriteLog("Entry", "fnGetDeviceParaList_CreateTbl", "Info");
        //    try
        //    {
        //        lstParaColumnNm = new List<string>();
        //        int iRows = dtParamterData.Rows.Count;
        //        for (int i = 0; i < iRows; i++)
        //        {
        //            if (iRows > 0)
        //            {
        //                // lststrDevParaList.Add(Convert.ToString(dtDevParameters.Rows[i]["ParameterName"]));
        //                lstParaColumnNm.Add(Convert.ToString(dtParamterData.Rows[i]["ParameterName"]).Replace(" ", "_"));
        //            }
        //        }
        //        //strTableName = strDevNm;
        //        fnCreateTable(dbname, strDevNm, dtParamterData, strIsPartCnt);
        //    }
        //    catch (Exception ex)
        //    {
        //        objclsLogger.fnWriteLog(ex.Message + "####" + ex.InnerException, "fnGetDeviceParaList_CreateTbl", "Error");

        //    }
        //    objclsLogger.fnWriteLog("Exit", "fnGetDeviceParaList_CreateTbl", "Info");
        //    return lstParaColumnNm;
        //}

        #region Hide 27.02.2020

        //public void fnGetAllDetails(string strdbname, string strQuery, out DataTable dtResultData)
        //{
        //    objclsLogger.fnWriteLog("Entry", "fnGetAllDetails", "Info");
        //    // objLogger.fnWriteLog("Entry Cloud DataLayer", "fnGetAllDetails", "Info");
        //    // strdatabase = strdbname + ".db";
        //    //objLogger.fnWriteLog("Database Cloud***^^^^^^=" + strdatabase, "fnGetAllDetails", "Error");
        //    dtResultData = new DataTable();

        //    try
        //    {
        //        //using (SQLiteConnection CloudCon = new SQLiteConnection("Data Source=" + Application.StartupPath + "\\" + strdbname + ".db" + ";Version=3;"))
        //        using (SQLiteConnection CloudCon = new SQLiteConnection("Data Source=" + Application.StartupPath + "\\" + strdbname + ".db" + "; Version=3;Synchronous=Off;Temp Store=Memory;Journal Mode=WAL;Locking Mode=Exclusive;Page Size=4096;"))
        //        {
        //            CloudCon.Open();
        //            using (SQLiteDataAdapter CloudDataAdapter = new SQLiteDataAdapter(strQuery, CloudCon))
        //            {
        //                CloudDataAdapter.Fill(dtResultData);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objclsLogger.fnWriteLog(ex.Message + "###" + ex.InnerException, "fnGetAllDetails", "Error");
        //    }
        //    objclsLogger.fnWriteLog("Exit", "fnGetAllDetails", "Info");
        //}

        //public bool fnInsertUpdateDeleteData(string strdbname, string strtblName, string strQuery)
        //{
        //    objclsLogger.fnWriteLog("Entry", "fnInsertUpdateDeleteData", "Info");
        //    bool flag = false;
        //    try
        //    {
        //        //strdatabase = strdbname + ".db";
        //        //objLogger.fnWriteLog("Database ## Cloud DataLayer=" + strdatabase, "fnInsertUpdateDeleteData", "Error");
        //        //using (SQLiteConnection CloudCon = new SQLiteConnection("Data Source=" + Application.StartupPath + "\\" + strdbname + ".db" + ";Version=3"))
        //        //SQLiteConnection CloudCon = new SQLiteConnection("Data Source=" + Application.StartupPath + "\\" + strdbname + ".db" + ";Version=3");
        //        using (SQLiteConnection CloudCon = new SQLiteConnection("Data Source=" + Application.StartupPath + "\\" + strdbname + ".db" + ";Version=3;Synchronous=Off;Temp Store=Memory;Journal Mode=WAL;Locking Mode=Exclusive;Page Size=4096;"))
        //        {
        //            CloudCon.Open();
        //            //SQLiteCommand CloudCmd = new SQLiteCommand(strQuery, CloudCon);
        //            using (SQLiteTransaction transaction = CloudCon.BeginTransaction())
        //            {
        //                using (SQLiteCommand CloudCmd = new SQLiteCommand(strQuery, CloudCon))
        //                {
        //                    CloudCmd.ExecuteNonQuery();
        //                    flag = true;
        //                    CloudCmd.Dispose();
        //                }
        //                transaction.Commit();
        //            }
        //            CloudCon.Close();
        //            CloudCon.Dispose();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        objclsLogger.fnWriteLog(ex.Message + "###" + ex.InnerException, "fnInsertUpdateDeleteData", "Error");
        //    }
        //    objclsLogger.fnWriteLog("Exit", "fnInsertUpdateDeleteData", "Info");
        //    return flag;
        //}
        //public bool fnKloudQInsertUpdateDeleteData(string strdbname, string strtblName, string strQuery, DataTable dtParamters)
        //{
        //    objclsLogger.fnWriteLog("Entry", "fnKloudQInsertUpdateDeleteData", "Info");
        //    //SQLiteConnection CloudCon=null;
        //    //objLogger.fnWriteLog("Entry Cloud DataLayer", "fnInsertUpdateDeleteData", "Info");
        //    bool flag = false;
        //    try
        //    {

        //        strdatabase = strdbname + ".db";
        //        //objLogger.fnWriteLog("Database ## Cloud DataLayer=" + strdatabase, "fnInsertUpdateDeleteData", "Error");
        //        //using (SQLiteConnection CloudCon = new SQLiteConnection("Data Source=" + Application.StartupPath + "\\" + strdatabase + ";Version=3"))
        //        //SQLiteConnection CloudCon = new SQLiteConnection("Data Source=" + Application.StartupPath + "\\" + strdatabase + ";Version=3");
        //        using (SQLiteConnection CloudCon = new SQLiteConnection("Data Source=" + Application.StartupPath + "\\" + strdatabase + ";Version=3;Synchronous=Off;Temp Store=Memory;Journal Mode=WAL;Locking Mode=Exclusive;Page Size=4096;"))
        //        {
        //            CloudCon.Open();
        //            using (SQLiteTransaction transaction = CloudCon.BeginTransaction())
        //            {
        //                using (SQLiteCommand CloudCmd = new SQLiteCommand(strQuery, CloudCon))
        //                //SQLiteCommand CloudCmd = new SQLiteCommand(strQuery, CloudCon);
        //                {
        //                    CloudCmd.ExecuteNonQuery();
        //                    flag = true;
        //                    CloudCmd.Dispose();
        //                }
        //                transaction.Commit();
        //            }
        //            CloudCon.Close();
        //            CloudCon.Dispose();
        //        }
        //    }
        //    catch (SQLiteException sqliteEx)
        //    {
        //        flag = false;
        //        objclsLogger.fnWriteLog("SQLITE ERROR DBMalformed of Lock " + sqliteEx.Message + " ### " + sqliteEx.InnerException, "fnKloudQInsertUpdateDeleteData", "Error");
        //        if (sqliteEx.ErrorCode == 11 || sqliteEx.ErrorCode == 5)
        //        {
        //            fnCreateDatabaseFile(strdbname, strtblName, dtParamters);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        objclsLogger.fnWriteLog(ex.Message + "###" + ex.InnerException, "fnKloudQInsertUpdateDeleteData", "Error");
        //    }
        //    objclsLogger.fnWriteLog("Exit", "fnKloudQInsertUpdateDeleteData", "Info");
        //    return flag;
        //}
        #endregion
        //List<string> lstParameterColumnList;

        //public void fnCreateDatabaseFile(string dbname, string tblName, DataTable dtParameter)
        //{
        //    objclsLogger.fnWriteLog("Entry", "fnCreateDatabaseFile", "Info");
        //    if (System.IO.File.Exists(Application.StartupPath + "\\" + dbname + ".db"))
        //    {
        //        File.Delete(Application.StartupPath + "\\" + dbname + ".db");
        //        lstParameterColumnList = fnGetDeviceParaList_CreateTbl(dbname, tblName, dtParameter, "");
        //        fnCreateProductionTable(tblName, dbname);
        //    }
        //    objclsLogger.fnWriteLog("Exit", "fnCreateDatabaseFile", "Info");
        //}
        //public void fnCloudGetAllDetails(string strdbname, string strQuery, out DataTable dtResultData)
        //{
        //    objclsLogger.fnWriteLog("Entry", "fnCloudGetAllDetails", "Info");
        //    dtResultData = new DataTable();
        //    try
        //    {
        //        if (CloudConnection.State != ConnectionState.Open)
        //        {
        //            CloudConnection.Open();
        //        }
        //        CloudDataAdapter = new SQLiteDataAdapter(strQuery, CloudConnection);
        //        CloudDataAdapter.Fill(dtResultData);
        //    }
        //    catch (Exception ex)
        //    {
        //        objclsLogger.fnWriteLog(ex.Message + "###" + ex.InnerException, "fnCloudGetAllDetails", "Error");
        //    }
        //    objclsLogger.fnWriteLog("Exit", "fnCloudGetAllDetails", "Info");
        //}



        //public bool fnCloudInsertUpdateDeleteData(string strdbname, string strtblName, string strQuery)
        //{
        //    objclsLogger.fnWriteLog("Entry", "fnCloudInsertUpdateDeleteData", "Info");
        //    bool flag = false;
        //    try
        //    {
        //        if (CloudConnection.State != ConnectionState.Open)
        //        {
        //            CloudConnection.Open();
        //        }
        //        //SQLiteCommand CloudCmd = new SQLiteCommand(strQuery, CloudCon);
        //        SQLiteTransaction transaction = CloudConnection.BeginTransaction();
        //        {
        //            CloudCmd = new SQLiteCommand(strQuery, CloudConnection);
        //            {
        //                CloudCmd.ExecuteNonQuery();
        //                flag = true;
        //                CloudCmd.Dispose();
        //            }
        //            transaction.Commit();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        objclsLogger.fnWriteLog(ex.Message + "###" + ex.InnerException, "fnCloudInsertUpdateDeleteData", "Error");
        //    }
        //    objclsLogger.fnWriteLog("Exit", "fnCloudInsertUpdateDeleteData", "Info");
        //    return flag;
        //}

        //public bool fnKloudQInsertUpdateDeleteData(string strdbname, string strtblName, string strQuery, DataTable dtParamters)
        //{
        //    objclsLogger.fnWriteLog("Entry", "fnKloudQInsertUpdateDeleteData", "Info");
        //    bool flag = false;
        //    try
        //    {
        //        if (CloudConnection.State != ConnectionState.Open)
        //        {
        //            CloudConnection.Open();
        //        }
        //        SQLiteTransaction transaction = CloudConnection.BeginTransaction();
        //        {
        //            CloudCmd = new SQLiteCommand(strQuery, CloudConnection);
        //            //SQLiteCommand CloudCmd = new SQLiteCommand(strQuery, CloudCon);
        //            {
        //                CloudCmd.ExecuteNonQuery();
        //                flag = true;
        //                CloudCmd.Dispose();
        //            }
        //            transaction.Commit();
        //        }
        //    }
        //    catch (SQLiteException sqliteEx)
        //    {
        //        flag = false;
        //        objclsLogger.fnWriteLog("SQLITE ERROR DBMalformed of Lock " + sqliteEx.Message + " ### " + sqliteEx.InnerException, "fnKloudQInsertUpdateDeleteData", "Error");
        //        if (sqliteEx.ErrorCode == 11 || sqliteEx.ErrorCode == 5)
        //        {
        //            fnCreateDatabaseFile(strdbname, strtblName, dtParamters);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        objclsLogger.fnWriteLog(ex.Message + "###" + ex.InnerException, "fnKloudQInsertUpdateDeleteData", "Error");
        //    }
        //    objclsLogger.fnWriteLog("Exit", "fnKloudQInsertUpdateDeleteData", "Info");
        //    return flag;
        //}
        #endregion

    }
}
